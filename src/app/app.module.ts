import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home/home.component';
import { SideComponent } from './side/side.component';
import { FooterComponent } from './footer/footer/footer.component';
import { BannerComponent } from './home/banner/banner.component';
import { PopularDestinationComponent } from './home/popular-destination/popular-destination.component';
import { FeaturedTravelComponent } from './home/featured-travel/featured-travel.component';
import { WhyUsComponent } from './home/why-us/why-us.component';
import { TestimonialsComponent } from './home/testimonials/testimonials.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideComponent,
    FooterComponent,
    BannerComponent,
    PopularDestinationComponent,
    FeaturedTravelComponent,
    WhyUsComponent,
    TestimonialsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
